#!/usr/bin/env python
"""
left arm's endpoint follow keyboard control example.
A demostration to use IKService and set_positions
Zhenghao Fei

"""
import argparse
import struct
import sys
import rospy
import baxter_interface
from baxter_interface import CHECK_VERSION

from geometry_msgs.msg import (
    PoseStamped,
    Pose,
    Point,
    Quaternion,
)

from std_msgs.msg import Header

from baxter_core_msgs.msg import EndpointState

from baxter_core_msgs.srv import (
    SolvePositionIK,
    SolvePositionIKRequest,
)


def callback(endpoint):
    hdr = Header(stamp=rospy.Time.now(), frame_id='base')

    poses = PoseStamped(
            header=hdr,
            pose=endpoint.pose
            )
    print(poses)

    # wait key board
    direction = input('please in put 1 to increase, or 2 decrease x: ')
    if direction == 1:
        print('x before: ', poses.pose.position.x)
        poses.pose.position.x = poses.pose.position.x + 0.05
        print('x now: ', poses.pose.position.x)
    if direction == 2:
        print('x before: ', poses.pose.position.x)
        poses.pose.position.x = poses.pose.position.x - 0.05
        print('x now: ', poses.pose.position.x)
    if direction == 3:
        print('y before: ', poses.pose.position.y)
        poses.pose.position.y = poses.pose.position.y + 0.05
        print('y now: ', poses.pose.position.y)
    if direction == 4:
        print('y before: ', poses.pose.position.y)
        poses.pose.position.y = poses.pose.position.y - 0.05
        print('y now: ', poses.pose.position.y)
    if direction == 5:
        print('z before: ', poses.pose.position.z)
        poses.pose.position.z = poses.pose.position.z + 0.05
        print('z now: ', poses.pose.position.z)
    if direction == 6:
        print('z before: ', poses.pose.position.z)
        poses.pose.position.z = poses.pose.position.z - 0.05
        print('z now: ', poses.pose.position.z)

    ns = "ExternalTools/" + 'left' + "/PositionKinematicsNode/IKService"
    iksvc = rospy.ServiceProxy(ns, SolvePositionIK)
    ikreq = SolvePositionIKRequest()
    ikreq.pose_stamp.append(poses)
    try:
        rospy.wait_for_service(ns, 5.0)
        resp = iksvc(ikreq)
    except (rospy.ServiceException, rospy.ROSException), e:
        rospy.logerr("Service call failed: %s" % (e,))
        return 1

    # Check if result valid, and type of seed ultimately used to get solution
    # convert rospy's string representation of uint8[]'s to int's
    resp_seeds = struct.unpack('<%dB' % len(resp.result_type),
                               resp.result_type)
    if (resp_seeds[0] != resp.RESULT_INVALID):
        seed_str = {
                    ikreq.SEED_USER: 'User Provided Seed',
                    ikreq.SEED_CURRENT: 'Current Joint Angles',
                    ikreq.SEED_NS_MAP: 'Nullspace Setpoints',
                   }.get(resp_seeds[0], 'None')
        print("SUCCESS - Valid Joint Solution Found from Seed Type: %s" %
              (seed_str,))
        # Format solution into Limb API-compatible dictionary
        limb_joints = dict(zip(resp.joints[0].name, resp.joints[0].position))
        print "\nIK Joint Solution:\n", limb_joints
        print "------------------"
        print "Response Message:\n", resp

        # go to position
        goto_position(resp, 'left')
        print('done')

    else:
        print("INVALID POSE - No Valid Joint Solution Found.")

def goto_position(resp, limb):
    # reformat the solution arrays into a dictionary
    joint_solution = dict(zip(resp.joints[0].name, resp.joints[0].position))
    # set arm joint positions to solution
    arm = baxter_interface.Limb(limb)
    arm.set_joint_positions(joint_solution)
    rospy.sleep(0.01)    

def listener():
    rospy.init_node("extend_arm")
    rospy.Subscriber("/robot/limb/left/commanded_endpoint_state", EndpointState, callback)
    rospy.spin()

if __name__ == '__main__':
    listener()
