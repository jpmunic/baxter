#!/usr/bin/env python
"""
follow the fruit position published by find fruit
try to put the fruit in the center of the camera and catch it
Current only work for left arm

Zhenghao

"""
import argparse
import struct
import sys
import rospy
import baxter_interface
import tf
import time
from baxter_interface import CHECK_VERSION


from grab_fruit.msg import fruit_pos
from geometry_msgs.msg import (
    PoseStamped,
    Pose,
    Point,
    Quaternion,
)
from std_msgs.msg import Header
from baxter_core_msgs.msg import EndpointState

from baxter_core_msgs.srv import (
    SolvePositionIK,
    SolvePositionIKRequest,
)

class extend_arm_node(object):
    def __init__(self):
        super(extend_arm_node, self).__init__()
        self.fruit_position_x = 0
        self.fruit_position_y = 0
        hdr = Header(stamp=rospy.Time.now(), frame_id='base')
        self.pose = PoseStamped(
            header=hdr,
            pose=Pose(
                position=Point(
                    x=0.953414412391,
                    y=0.599700624104,
                    z=0.500111747436,
                ),
                orientation=Quaternion(
                    x=0.0118953582747,
                    y=0.708807476673,
                    z=-0.0162654288192,
                    w=0.705114102319,
                ),
            ),
        )
    
    

    def reset(self):
        self.fruit_position_x = 0
        self.fruit_position_y = 0
        hdr = Header(stamp=rospy.Time.now(), frame_id='base')
        self.pose = PoseStamped(
            header=hdr,
            pose=Pose(
                position=Point(
                    x=0.953414412391,
                    y=0.599700624104,
                    z=0.500111747436,
                ),
                orientation=Quaternion(
                    x=0.0118953582747,
                    y=0.708807476673,
                    z=-0.0162654288192,
                    w=0.705114102319,
                ),
            ),
        )
        # self.arm_position_x=0.953414412391
        # self.arm_position_y=0.708807476673
        # self.arm_position_z=0.500111747436
        # self.arm_orientation_x=0.0118953582747
        # self.arm_orientation_y=0.708807476673
        # self.arm_orientation_z=-0.0162654288192
        # self.arm_orientation_w=0.705114102319



def callback_fruit(fruit_position, arm_node):
    print("new fruit pos")

    # update fruit position
    arm_node.fruit_position_x = fruit_position.x
    arm_node.fruit_position_y = fruit_position.y
    # arm_node.fruit_position_x = 320
    # arm_node.fruit_position_y = 200
    follow_fruit(arm_node)  #  follow the fruit


def follow_fruit(arm_node):

    move = False;
    # try to make the fruit towards center manipulate in camera frame
    if arm_node.fruit_position_y > 210:
        arm_node.pose.pose.position.y = arm_node.pose.pose.position.y + 0.01
        print('y +')
        move = True;
    if arm_node.fruit_position_y < 190:
        arm_node.pose.pose.position.y = arm_node.pose.pose.position.y - 0.01
        print('y - ')
        move = True;
    if arm_node.fruit_position_x > 330:
        arm_node.pose.pose.position.z = arm_node.pose.pose.position.z + 0.01
        print('z +')
        move = True;
    if arm_node.fruit_position_x < 310:
        arm_node.pose.pose.position.z = arm_node.pose.pose.position.z - 0.01
        print('z - ')    
        move = True;

    # if fruit is at center catch it!
    if move == False:
        arm_node.pose.pose.position.x = arm_node.pose.pose.position.x + 0.01
        print('x +')
    
    keep_time = 0.1
    invers_kinematic(arm_node.pose, keep_time)


def invers_kinematic(aim_pose, keep_time):
    ns = "ExternalTools/" + 'left' + "/PositionKinematicsNode/IKService"
    iksvc = rospy.ServiceProxy(ns, SolvePositionIK)
    ikreq = SolvePositionIKRequest()
    ikreq.pose_stamp.append(aim_pose)
    try:
        rospy.wait_for_service(ns, 5.0)
        resp = iksvc(ikreq)
    except (rospy.ServiceException, rospy.ROSException), e:
        rospy.logerr("Service call failed: %s" % (e,))
        return 1

    # Check if result valid, and type of seed ultimately used to get solution
    # convert rospy's string representation of uint8[]'s to int's
    resp_seeds = struct.unpack('<%dB' % len(resp.result_type),
                               resp.result_type)
    if (resp_seeds[0] != resp.RESULT_INVALID):
        seed_str = {
                    ikreq.SEED_USER: 'User Provided Seed',
                    ikreq.SEED_CURRENT: 'Current Joint Angles',
                    ikreq.SEED_NS_MAP: 'Nullspace Setpoints',
                   }.get(resp_seeds[0], 'None')
        # print("SUCCESS - Valid Joint Solution Found from Seed Type: %s" %
        #       (seed_str,))
        # Format solution into Limb API-compatible dictionary
        limb_joints = dict(zip(resp.joints[0].name, resp.joints[0].position))
        # print "\nIK Joint Solution:\n", limb_joints
        # print "------------------"
        # print "Response Message:\n", resp

        # go to position
        goto_position(resp, 'left', keep_time)
        print('done')

    else:
        print("INVALID POSE - No Valid Joint Solution Found.")

def goto_position(resp, limb, keep_time):
    # reformat the solution arrays into a dictionary
    joint_solution = dict(zip(resp.joints[0].name, resp.joints[0].position))
    # set arm joint positions to solution
    arm = baxter_interface.Limb(limb)
    tic = time.time()
    toc = tic
    print('go')
    while toc - tic < keep_time:
        arm.set_joint_positions(joint_solution)
        toc = time.time()
    print('leave')

    rospy.sleep(0.01)    

def listener():
    rospy.init_node("extend_arm")
    arm_node = extend_arm_node()
    print("reset to start point")
    keep_time = 5 # 5 sec to let arm reset
    invers_kinematic(arm_node.pose, keep_time)
    rospy.Subscriber("/fruit_position", fruit_pos, callback_fruit, arm_node)
    rospy.spin()

if __name__ == '__main__':
    listener()
