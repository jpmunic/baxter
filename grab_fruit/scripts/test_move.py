#!/usr/bin/env python


"""
Baxter RSDK Inverse Kinematics and move test
"""
import argparse
import struct
import sys
import rospy
import baxter_interface
import tf
import time
from baxter_interface import CHECK_VERSION


from grab_fruit.msg import fruit_pos
from geometry_msgs.msg import (
    PoseStamped,
    Pose,
    Point,
    Quaternion,
)
from std_msgs.msg import Header
from baxter_core_msgs.msg import EndpointState

from baxter_core_msgs.srv import (
    SolvePositionIK,
    SolvePositionIKRequest,
)

def ik_test(limb):
    rospy.init_node("rsdk_ik_service_client")
    ns = "ExternalTools/" + limb + "/PositionKinematicsNode/IKService"
    iksvc = rospy.ServiceProxy(ns, SolvePositionIK)
    ikreq = SolvePositionIKRequest()
    hdr = Header(stamp=rospy.Time.now(), frame_id='base')
    poses = {
        'left': PoseStamped(
            header=hdr,
            pose=Pose(
                # position=Point(
                #     x=0.657579481614,
                #     y=0.851981417433,
                #     z=0.0388352386502,
                # ),
                # orientation=Quaternion(
                #     x=-0.366894936773,
                #     y=0.885980397775,
                #     z=0.108155782462,
                #     # w=0.262162481772,
                #     w=1.0,
                # ),
                position=Point(
                    x=0.953414412391,
                    y=0.599700624104,
                    z=0.500111747436,
                ),
                orientation=Quaternion(
                    x=0.0118953582747,
                    y=0.708807476673,
                    z=-0.0162654288192,
                    # w=0.262162481772,
                    w=0.705114102319,
                ),
            ),
        ),
        'right': PoseStamped(
            header=hdr,
            pose=Pose(
                position=Point(
                    x=0.656982770038,
                    y=-0.852598021641,
                    z=0.0388609422173,
                ),
                orientation=Quaternion(
                    x=0.367048116303,
                    y=0.885911751787,
                    z=-0.108908281936,
                    w=0.261868353356,
                ),
            ),
        ),
    }

    for i in range(5):
        # poses[limb].pose.position.x -= 0.1
        # # print('x = ', poses[limb].pose.position.x)
        # poses[limb].pose.position.x = 0.95
        # poses[limb].pose.position.y = 0.6
        poses[limb].pose.position.z += 0.1

        # # poses[limb].pose.orientation.x = 0
        # poses[limb].pose.orientation.y -= 0.1
        # # poses[limb].pose.orientation.z = 0
        # poses[limb].pose.orientation.z = 0
        # poses[limb].pose.orientation.w = 1

        # quaternion = (
        # poses[limb].pose.orientation.x,
        # poses[limb].pose.orientation.y,
        # poses[limb].pose.orientation.z,
        # poses[limb].pose.orientation.w)
        # euler = tf.transformations.euler_from_quaternion(quaternion)
        # print(euler)
        # euler = (0, 0, 0)
        # quaternion = tf.transformations.quaternion_from_euler(euler[0], euler[1], euler[2])
        # print(quaternion)





        ikreq.pose_stamp.append(poses[limb])
        try:
            rospy.wait_for_service(ns, 5.0)
            resp = iksvc(ikreq)
        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))
            return 1

        # Check if result valid, and type of seed ultimately used to get solution
        # convert rospy's string representation of uint8[]'s to int's
        resp_seeds = struct.unpack('<%dB' % len(resp.result_type),
                                   resp.result_type)
        if (resp_seeds[0] != resp.RESULT_INVALID):
            seed_str = {
                        ikreq.SEED_USER: 'User Provided Seed',
                        ikreq.SEED_CURRENT: 'Current Joint Angles',
                        ikreq.SEED_NS_MAP: 'Nullspace Setpoints',
                       }.get(resp_seeds[0], 'None')
            # print("SUCCESS - Valid Joint Solution Found from Seed Type: %s" % (seed_str,))
            # Format solution into Limb API-compatible dictionary
            limb_joints = dict(zip(resp.joints[0].name, resp.joints[0].position))
            # print "\nIK Joint Solution:\n", limb_joints
            # print "------------------"
            # print "Response Message:\n", resp

            goto_position(resp, limb)


        else:
            print("INVALID POSE - No Valid Joint Solution Found.")
        

    return 0

def goto_position(resp, limb):
    # reformat the solution arrays into a dictionary
    joint_solution = dict(zip(resp.joints[0].name, resp.joints[0].position))
    # set arm joint positions to solution
    arm = baxter_interface.Limb(limb)
    tic = time.time()
    toc = tic
    print('go')
    while toc - tic < 1:
        arm.set_joint_positions(joint_solution)
        toc = time.time()
    print('leave')

    rospy.sleep(0.01)   

def main():
    """RSDK Inverse Kinematics Example

    A simple example of using the Rethink Inverse Kinematics
    Service which returns the joint angles and validity for
    a requested Cartesian Pose.

    Run this example, passing the *limb* to test, and the
    example will call the Service with a sample Cartesian
    Pose, pre-defined in the example code, printing the
    response of whether a valid joint solution was found,
    and if so, the corresponding joint angles.
    """
    arg_fmt = argparse.RawDescriptionHelpFormatter
    parser = argparse.ArgumentParser(formatter_class=arg_fmt,
                                     description=main.__doc__)
    parser.add_argument(
        '-l', '--limb', choices=['left', 'right'], required=True,
        help="the limb to test"
    )
    args = parser.parse_args(rospy.myargv()[1:])

    return ik_test(args.limb)

if __name__ == '__main__':
    sys.exit(main())
