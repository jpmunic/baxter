#!/usr/bin/env python

import argparse
import struct
import sys
import rospy
import baxter_interface
import tf
from baxter_interface import CHECK_VERSION


from grab_fruit.msg import fruit_pos
from geometry_msgs.msg import (
    PoseStamped,
    Pose,
    Point,
    Quaternion,
)
from std_msgs.msg import Header
from baxter_core_msgs.msg import EndpointState

from baxter_core_msgs.srv import (
    SolvePositionIK,
    SolvePositionIKRequest,
)

def callback_fruit(pose, tl):

    tl.waitForTransform('/base', '/left_hand_camera', rospy.Time(), rospy.Duration(1.0))

    try:
        (trans,rot) = tl.lookupTransform('/base', '/left_hand_camera', rospy.Time())
        print (trans)

    except (tf.LookupException, tf.ConnectivityException):
        pass 

if __name__ == '__main__':
    rospy.init_node("lalala")
    tl = tf.TransformListener() # tf listener

    rospy.Subscriber("/fruit_position", fruit_pos, callback_fruit, tl)
    # rospy.Subscriber("/robot/limb/left/commanded_endpoint_state", EndpointState, callback_arm)

    # # rate = rospy.Rate(10.0)

    rospy.spin()

