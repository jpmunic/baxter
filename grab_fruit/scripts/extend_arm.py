#!/usr/bin/env python
"""
Extend arm to follow the fruit position published by find fruit
Current only work for left arm

Zhenghao

"""
import argparse
import struct
import sys
import rospy
import baxter_interface
import tf
import time
from baxter_interface import CHECK_VERSION


from grab_fruit.msg import fruit_pos
from geometry_msgs.msg import (
    PoseStamped,
    Pose,
    Point,
    Quaternion,
)
from std_msgs.msg import Header
from baxter_core_msgs.msg import EndpointState

from baxter_core_msgs.srv import (
    SolvePositionIK,
    SolvePositionIKRequest,
)

class extend_arm_node(object):
    def __init__(self):
        super(extend_arm_node, self).__init__()
        self.fruit_position_x = 0
        self.fruit_position_y = 0
        self.arm_position_x=0;
        self.arm_position_y=0;
        self.arm_position_z=0;
        self.arm_orientation_x=0;
        self.arm_orientation_y=0;
        self.arm_orientation_z=0;
        self.arm_orientation_w=0;    

# global msgs
arm_node = extend_arm_node()

def callback_fruit(fruit_position):
    global arm_node

    arm_node.fruit_position_x = fruit_position.x
    arm_node.fruit_position_y = fruit_position.y
    limb = 'left'
    arm = baxter_interface.Limb(limb)
    pose_base = arm.endpoint_pose()
    print(pose_base)
    # print('fruit_position: ' ,arm_node.fruit_position_x, ', ', arm_node.fruit_position_y)

def callback_arm(endpoint_state, tl):
    global arm_node

    # arm_node.arm_position_x = endpoint_state.pose.position.x;
    # arm_node.arm_position_y = endpoint_state.pose.position.y;
    # arm_node.arm_position_z = endpoint_state.pose.position.z;
    # arm_node.arm_orientation_x = endpoint_state.pose.orientation.x;
    # arm_node.arm_orientation_y = endpoint_state.pose.orientation.y;
    # arm_node.arm_orientation_z = endpoint_state.pose.orientation.z;
    # arm_node.arm_orientation_w = endpoint_state.pose.orientation.w;

    follow_fruit(endpoint_state, tl)  #  follow the fruit
    # print(endpoint_state)


def follow_fruit(endpoint_state, tl):
    global arm_node

    # construct pose msg
    now = rospy.Time(0)
    hdr = Header(stamp=now, frame_id='/base')

    pose_base = PoseStamped(
            header=hdr,
            pose=endpoint_state.pose
            )
    print(pose_base)

    # tl.waitForTransform('/base', '/left_hand_camera', now, rospy.Duration(1.0))

    # try:
    #     (trans,rot) = tl.lookupTransform('/base', '/left_hand_camera', rospy.Time())
    #     pose_camera= tl.transformPose('/left_hand_camera', pose_base)

    #     # print (trans)
    #     # print('b 2 c')

    # except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
    #     pass 

    # tf transformation base to camera
    # tl.waitForTransform('/base', '/left_hand_camera', rospy.Time(), rospy.Duration(4.0))
    # print(pose_base)



    move = False;
    # try to make the fruit towards center manipulate in camera frame
    # if arm_node.fruit_position_y > 210:
    #     pose_camera.pose.position.y = pose_camera.pose.position.y + 0.05
    #     print('y +')
    #     move = True;
    # if arm_node.fruit_position_y < 190:
    #     pose_camera.pose.position.y = pose_camera.pose.position.y - 0.05
    #     print('y - ')
    #     move = True;
    # if arm_node.fruit_position_x > 330:
    #     pose_camera.pose.position.x = pose_camera.pose.position.x + 0.05
    #     print('x +')
    #     move = True;
    # if arm_node.fruit_position_x < 310:
    #     pose_camera.pose.position.x = pose_camera.pose.position.x - 0.05
    #     print('x - ')    
    #     move = True;


    # pose_base.pose.position.x = 0.349813704313
    # pose_base.pose.position.y = 0.542523446127
    # pose_base.pose.position.z = 0.171771069977
    # pose_base.pose.orientation.x = 0.827896554515
    # pose_base.pose.orientation.y = -0.204379148589
    # pose_base.pose.orientation.z = 0.4941953970
    # pose_base.pose.orientation.w = -0.169077994504

    print(pose_base.pose.position.x)
    print('x + ')
    move = True;
    invers_kinematic(pose_base)

    # if move:
    #     # tf transformation from camera back to base
    #     try:
    #         # now = rospy.Time.now()
    #         tl.waitForTransform('/left_hand_camera', '/base', now, rospy.Duration(4.0))
    #         pose_base= tl.transformPose('/base', pose_camera)
    #         print('c 2 b')
    #     except (tf.LookupException, tf.ConnectivityException):
    #         print('failed')

    #     invers_kinematic(pose_base)
    #     print('move')

    # else:
    #     print('hand arrived')

def invers_kinematic(aim_pose):
    ns = "ExternalTools/" + 'left' + "/PositionKinematicsNode/IKService"
    iksvc = rospy.ServiceProxy(ns, SolvePositionIK)
    ikreq = SolvePositionIKRequest()
    ikreq.pose_stamp.append(aim_pose)
    try:
        rospy.wait_for_service(ns, 5.0)
        resp = iksvc(ikreq)
    except (rospy.ServiceException, rospy.ROSException), e:
        rospy.logerr("Service call failed: %s" % (e,))
        return 1

    # Check if result valid, and type of seed ultimately used to get solution
    # convert rospy's string representation of uint8[]'s to int's
    resp_seeds = struct.unpack('<%dB' % len(resp.result_type),
                               resp.result_type)
    if (resp_seeds[0] != resp.RESULT_INVALID):
        seed_str = {
                    ikreq.SEED_USER: 'User Provided Seed',
                    ikreq.SEED_CURRENT: 'Current Joint Angles',
                    ikreq.SEED_NS_MAP: 'Nullspace Setpoints',
                   }.get(resp_seeds[0], 'None')
        # print("SUCCESS - Valid Joint Solution Found from Seed Type: %s" %
        #       (seed_str,))
        # Format solution into Limb API-compatible dictionary
        limb_joints = dict(zip(resp.joints[0].name, resp.joints[0].position))
        # print "\nIK Joint Solution:\n", limb_joints
        # print "------------------"
        # print "Response Message:\n", resp

        # go to position
        goto_position(resp, 'left')
        print('done')

    else:
        print("INVALID POSE - No Valid Joint Solution Found.")

def goto_position(resp, limb):
    # reformat the solution arrays into a dictionary
    joint_solution = dict(zip(resp.joints[0].name, resp.joints[0].position))
    # set arm joint positions to solution
    arm = baxter_interface.Limb(limb)
    tic = time.time()
    toc = tic
    print('go')
    while toc - tic < 5:
        arm.set_joint_positions(joint_solution)
        toc = time.time()
    print('leave')

    rospy.sleep(0.01)    

def listener():
    rospy.init_node("extend_arm")
    tl = tf.TransformListener() # tf listener

    rospy.Subscriber("/fruit_position", fruit_pos, callback_fruit)
    # rospy.Subscriber("/robot/limb/left/endpoint_state", EndpointState, callback_arm, tl, queue_size = 1)




    rospy.spin()

if __name__ == '__main__':
    listener()
