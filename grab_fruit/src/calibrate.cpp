/*
For making opencv work with ros:
http://wiki.ros.org/image_transport/Tutorials/SubscribingToImages

For color detection:
http://opencv-srf.blogspot.com/2010/09/object-detection-using-color-seperation.html

*/
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <cv_bridge/cv_bridge.h>
#include <iostream>
#include "HSV_limits.h"
#include "calibrate.h"

//main function
int main(int argc, char **argv)
{
	ros::init(argc, argv, "image_listener");
	calibrate_node calib_node;
	calib_node.start();//does not return until the node is shut down
	return 0;
}
//created by Josh Munic (jpmunic@gmail.com) in 2016
