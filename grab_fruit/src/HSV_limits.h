/*
DESCRIPTION:
	struct that holds the minimum and maximum HSV values for a fruit detection.
AUTHOR:
	Josh Munic
E-MAIL:
	jpmunic@gmail.com
*/

#ifndef HSV_LIMITS_H
#define HSV_LIMITS_H

#include <fstream>
#include <string>
#include <stdlib.h>
#include <iostream>
#include "package_path.h"

//declare structs and classes
struct HSV_limits
{
	int LowH; 
	int HighH;
	int LowS;
	int HighS;
	int LowV;
	int HighV;
	HSV_limits():
	LowH(0),
	HighH(179),
	LowS(0),
	HighS(255),
	LowV(0),
	HighV(255)
	{
		//reads in values from the file
		std::string line;
		char path[100];
		strcpy(path,path_to_package);
		strcat(path,"/color_limits.txt");
		std::ifstream text_file(path);
		if (text_file.is_open())
		{
			std::getline(text_file,line);
			LowH=atoi(line.c_str());
			std::getline(text_file,line);
			HighH=atoi(line.c_str());
			std::getline(text_file,line);
			LowS=atoi(line.c_str());
			std::getline(text_file,line);
			HighS=atoi(line.c_str());
			std::getline(text_file,line);
			LowV=atoi(line.c_str());
			std::getline(text_file,line);
			HighV=atoi(line.c_str());
			text_file.close();
		}
		else
			std::cout<<"no default color values"<<std::endl;
	}

	~HSV_limits()
	{
		//writes the values to the file
		char path[100];
		strcpy(path,path_to_package);
		strcat(path,"/color_limits.txt");
		std::ofstream text_file(path);
		if (text_file.is_open())
		{
			text_file<<LowH<<std::endl;
			text_file<<HighH<<std::endl;
			text_file<<LowS<<std::endl;
			text_file<<HighS<<std::endl;
			text_file<<LowV<<std::endl;
			text_file<<HighV<<std::endl;
			text_file.close();
		}
		else
			std::cout<<"couldn't write to the file"<<std::endl;	
	}
};
#endif
