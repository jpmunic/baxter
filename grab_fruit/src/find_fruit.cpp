/*
created by Josh Munic in 2016

to compile: catkin_make --pkg grab_fruit

For making opencv work with ros:
http://wiki.ros.org/image_transport/Tutorials/SubscribingToImages

For color detection:
http://opencv-srf.blogspot.com/2010/09/object-detection-using-color-seperation.html
*/
#include "find_fruit.h"


//main function
int main(int argc, char **argv)
{
	ros::init(argc, argv, "image_listener");
	find_fruit_node node;
	node.start();
	return 0;
}

//created by Josh Munic (jpmunic@gmail.com) in 2016
