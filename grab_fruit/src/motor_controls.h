/*
	Commands to control Baxter's motors. 
*/

#ifndef MOTOR_CONTROLS_H
#define MOTOR_CONTROLS_H

#include <ros/ros.h>
#include <iostream> //cout
#include <math.h> //round
#include <vector>
#include "std_msgs/Bool.h"
#include "std_msgs/String.h"
#include "JointCommand.h"

class Motor_controls{
    public:
	Motor_controls();
	void enable();
	void disable();
	//publishes once
	void move_left_arm(const std::vector<double> & joint_positions); 
	void move_right_arm(const std::vector<double> & joint_positions); 
	//publishes for a specific amount of time
	void move_left_arm(const std::vector<double> & joint_positions,const float & seconds); //seconds to publish 
	void move_right_arm(const std::vector<double> & joint_positions,const float & seconds); //seconds to publish 

    private:
	void toggle_enabled(const bool); //call with true to enable, and with false to disable robot
	baxter_core_msgs::JointCommand left_joint_command; 
	baxter_core_msgs::JointCommand right_joint_command; 
	ros::Publisher left_cmd_pub;
	ros::Publisher right_cmd_pub;
};

Motor_controls::Motor_controls(){
	//set up a joint command template for sending joint commands to the left arm

	// put the command in velocity mode
	left_joint_command.mode = baxter_core_msgs::JointCommand::POSITION_MODE;
	// command joints in the order shown in baxter_interface
	left_joint_command.names.push_back("left_s0");
	left_joint_command.names.push_back("left_s1");
	left_joint_command.names.push_back("left_e0"); //rotate shoulder. + is right, - is left
	left_joint_command.names.push_back("left_e1"); //raise arm. + is down, - is up
	left_joint_command.names.push_back("left_w0"); //rotate arm. + is left, - is right
	left_joint_command.names.push_back("left_w1"); //curl elbow. + is inwards, - is outwards
	left_joint_command.names.push_back("left_w2"); //rotate wrist. + is left, - is right
	//resize the amount of commands to the amount of names
	left_joint_command.command.resize(left_joint_command.names.size());

	//create the left arm joint command publisher
	ros::NodeHandle left_nh;
	left_cmd_pub = left_nh.advertise<baxter_core_msgs::JointCommand>("/robot/limb/left/joint_command", 1);
	
	//set up a joint command template for sending joint commands to the right arm

	// put the command in velocity mode
	right_joint_command.mode = baxter_core_msgs::JointCommand::POSITION_MODE;
	// command joints in the order shown in baxter_interface
	right_joint_command.names.push_back("right_s0");
	right_joint_command.names.push_back("right_s1");
	right_joint_command.names.push_back("right_e0"); //rotate shoulder. + is right, - is left
	right_joint_command.names.push_back("right_e1"); //raise arm. + is down, - is up
	right_joint_command.names.push_back("right_w0"); //rotate arm. + is left, - is right
	right_joint_command.names.push_back("right_w1"); //curl elbow. + is inwards, - is outwards
	right_joint_command.names.push_back("right_w2"); //rotate wrist. + is left, - is right
	//resize the amount of commands to the amount of names
	right_joint_command.command.resize(right_joint_command.names.size());

	//create the right arm joint command publisher
	ros::NodeHandle right_nh;
	right_cmd_pub = right_nh.advertise<baxter_core_msgs::JointCommand>("/robot/limb/right/joint_command", 1);
}

void Motor_controls::enable(){
	toggle_enabled(true);
}

void Motor_controls::disable(){
	toggle_enabled(false);
}

//sends one command
void Motor_controls::move_left_arm(const std::vector<double> & joint_positions){
	const int joint_count=7;
	//check input validity
	if (joint_positions.size()!=joint_count){
		std::cout<<"ERROR: input vector must be have "<<joint_count<<" entries"<<std::endl;
		return;
	}

	//set the joint command equal to the input joint positions
	for (int i=0;i<7;i++){
		left_joint_command.command[i]=joint_positions[i];
	}

	//publish the joint command
	left_cmd_pub.publish(left_joint_command);
}

void Motor_controls::move_right_arm(const std::vector<double> & joint_positions){
	const int joint_count=7;
	//check input validity
	if (joint_positions.size()!=joint_count){
		std::cout<<"ERROR: input vector must be have "<<joint_count<<" entries"<<std::endl;
		return;
	}

	//set the joint command equal to the input joint positions
	for (int i=0;i<7;i++){
		right_joint_command.command[i]=joint_positions[i];
	}

	//publish the joint command
	right_cmd_pub.publish(right_joint_command);
}

//sends a movement command for and input amount of seconds
void Motor_controls::move_left_arm(const std::vector<double> & joint_positions,const float & seconds){
	const int joint_count=7;
	const float minimum_seconds=.1;
	//check input validity
	if (joint_positions.size()!=joint_count){
		std::cout<<"ERROR: input vector must be have "<<joint_count<<" entries"<<std::endl;
		return;
	}
	if (seconds<minimum_seconds){
		std::cout<<"ERROR: input time must be greater than "<<minimum_seconds<<std::endl;
		return;
	}

	// publish at at least 5 Hz, or else Baxter switches back to Position mode and holds position
	const unsigned int frequency=10; //10 Hz
	ros::Rate loop_rate(frequency); 
	
	//set the joint command equal to the input joint positions
	for (int i=0;i<7;i++){
		left_joint_command.command[i]=joint_positions[i];
	}

	unsigned int max_count=round(seconds*frequency);

	unsigned int count=0;
	while(ros::ok() && count<max_count){
		left_cmd_pub.publish(left_joint_command);
		ros::spinOnce();
		loop_rate.sleep();
		count++;
	}
}

void Motor_controls::move_right_arm(const std::vector<double> & joint_positions,const float & seconds){
	const int joint_count=7;
	const float minimum_seconds=.1;
	//check input validity
	if (joint_positions.size()!=joint_count){
		std::cout<<"ERROR: input vector must be have "<<joint_count<<" entries"<<std::endl;
		return;
	}
	if (seconds<minimum_seconds){
		std::cout<<"ERROR: input time must be greater than "<<minimum_seconds<<std::endl;
		return;
	}

	// publish at at least 5 Hz, or else Baxter switches back to Position mode and holds position
	const unsigned int frequency=10; //10 Hz
	ros::Rate loop_rate(frequency); 
	
	//set the joint command equal to the input joint positions
	for (int i=0;i<7;i++){
		right_joint_command.command[i]=joint_positions[i];
	}

	unsigned int max_count=round(seconds*frequency);

	unsigned int count=0;
	while(ros::ok() && count<max_count){
		right_cmd_pub.publish(right_joint_command);
		ros::spinOnce();
		loop_rate.sleep();
		count++;
	}
}

//pass in true to enable joint movement, and false to disable joint movement.
void Motor_controls::toggle_enabled(const bool status){
	ros::NodeHandle n;
	ros::Publisher enable_pub = n.advertise<std_msgs::Bool>("robot/set_super_enable", 1000);
	ros::Rate loop_rate(10); //10 hz
	unsigned int count=0;
	while (ros::ok() && count<10)
	{
		std_msgs::Bool msg;
		msg.data=status;
		enable_pub.publish(msg);
		ros::spinOnce();
		loop_rate.sleep();
		count++;
	}
}
#endif
