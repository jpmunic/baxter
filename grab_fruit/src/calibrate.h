/*
	Definition of the calibrate_node class
	Contains the HSV_limits class from HSV_limits.h
	calibrate_node class is created in calibrate.cpp
*/
#ifndef CALIBRATE_H
#define CALIBRATE_H

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <cv_bridge/cv_bridge.h>
#include "HSV_limits.h"
#include <iostream>


class calibrate_node
{
    public:
	~calibrate_node();
	virtual void start();
    protected:
	//function prototypes
	void create_trackbars();
	void start_video_feed();
	void imageCallback(const sensor_msgs::ImageConstPtr&);

	//variables
	HSV_limits color_limits;
};

calibrate_node::~calibrate_node()
{
	cv::destroyWindow("view");
	cv::destroyWindow("Track Bars");
}

void calibrate_node::start(){
	create_trackbars();
	start_video_feed();
}

void calibrate_node::start_video_feed()
{
	cv::namedWindow("view");
	cv::startWindowThread();
	ros::NodeHandle nh;
	image_transport::ImageTransport it(nh);
	image_transport::Subscriber sub = it.subscribe("cameras/left_hand_camera/image", 1, &calibrate_node::imageCallback, this);
	ros::spin(); //does not return until the node is shutdown
}

//called when a new image arrives in the cameras/left_hand_camera/image topic
void calibrate_node::imageCallback(const sensor_msgs::ImageConstPtr& msg)
{
	//capture original image in bgr format
	//convert ros image to cv::Mat
	cv::Mat original_image=cv_bridge::toCvShare(msg, "bgr8")->image;
	//shows the camera feed before filtering
	//commented out because of issues with opening two camera feeds at once
	/*
	try
	{
		//show the original image in the view window
		cv::imshow("view",original_image); 
		cv::waitKey(30);
	}
		catch (cv_bridge::Exception& e)
	{
		ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
	}
	*/
	//convert the bgr image to HSV
	cv::Mat HSV_image;
	cv::cvtColor(original_image,HSV_image,cv::COLOR_BGR2HSV);  

	//create an image that is white when in the HSV limits, and black everywhere else
	cv::Mat thresh_image;
	cv::inRange(HSV_image,
	cv::Scalar(color_limits.LowH,color_limits.LowS,color_limits.LowV),
	cv::Scalar(color_limits.HighH,color_limits.HighS,color_limits.HighV),
	thresh_image);
	//morphological opening (remove small objects from the foreground)
	cv::erode( thresh_image,thresh_image,getStructuringElement(cv::MORPH_ELLIPSE,cv::Size(5,5)));
	cv::dilate(thresh_image,thresh_image,getStructuringElement(cv::MORPH_ELLIPSE,cv::Size(5,5))); 

	//morphological closing (fill small holes in the foreground)
	cv::dilate(thresh_image,thresh_image,getStructuringElement(cv::MORPH_ELLIPSE,cv::Size(5,5))); 
	cv::erode( thresh_image,thresh_image,getStructuringElement(cv::MORPH_ELLIPSE,cv::Size(5,5)));

	try
	{
		//show the threshholded image
		cv::imshow("view",thresh_image); 
		cv::waitKey(30);
	}
	catch (cv_bridge::Exception& e)
	{
		ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
	}

	//if you press x on the window, the node shuts down
	if (cv::getWindowProperty("view",1)<0 || cv::getWindowProperty("Track Bars",1)<0) //getWindowProperty returns -1 if there's no window
		ros::shutdown();
}

void calibrate_node::create_trackbars()
{
	//Create a window called "Track Bars"
	cv::namedWindow("Track Bars");
	//Create trackbars in "Track Bars" window
	cv::createTrackbar("LowH",  "Track Bars", &color_limits.LowH, 179); //Hue (0 - 179)
	cv::createTrackbar("HighH", "Track Bars", &color_limits.HighH, 179);
	cv::createTrackbar("LowS",  "Track Bars", &color_limits.LowS, 255); //Saturation (0 - 255)
	cv::createTrackbar("HighS", "Track Bars", &color_limits.HighS, 255);
	cv::createTrackbar("LowV",  "Track Bars", &color_limits.LowV, 255); //Value (0 - 255)
	cv::createTrackbar("HighV", "Track Bars", &color_limits.HighV, 255);
}

#endif
