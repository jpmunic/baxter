/*
	First gets into camera facing forward position.
	Second, activates the vaccum hand. 
	Third, reaches towards the "fruit" using information from the fruit_position topic.
	Finally, returns the "fruit" to baxter and turns off the vaccum hand. 
*/

#include <ros/ros.h>
#include "extend_arm.h"

int main(int argc, char **argv)
{
	ros::init(argc, argv, "get_in_position");
	Extend_arm_node extend_arm_node;
	extend_arm_node.start();
	return 0;
}
//Created by Josh Munic (jpmunic@gmail.com) in 2017
