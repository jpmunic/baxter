/*
	Definition of the get_in_position_node class, which is used in get_in_position.cpp

	The extend_arm_node class defined in extend_arm.h derives from this class	
*/

#ifndef GET_IN_POSITION_H
#define GET_IN_POSITION_H
#include <ros/ros.h>
#include <vector>
#include "motor_controls.h"

class get_in_position_node{
    public:
	~get_in_position_node();
	virtual void start();

    protected:
	Motor_controls motor_controls;

};

get_in_position_node::~get_in_position_node(){
	motor_controls.disable();
}

void get_in_position_node::start(){
	//enable the robot
	motor_controls.enable();
	
	std::vector<double> initial_joint_positions;
	//set the joint position to move to.
	//edit these values to change position.
	initial_joint_positions.push_back(-1.6866118762800124);
	initial_joint_positions.push_back(-2.1840051467518578);
	initial_joint_positions.push_back(2.447849842268149);
	initial_joint_positions.push_back(2.6181217097234297);
	initial_joint_positions.push_back(-1.254412789293297);
	initial_joint_positions.push_back(1.2321700678689587);
	initial_joint_positions.push_back(2.9594324350279346);
	//move to the inital joint posiiton
	const float publish_time=10;//seconds
	motor_controls.move_left_arm(initial_joint_positions,publish_time);
}

#endif
