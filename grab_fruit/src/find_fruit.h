/*
	Definition of the find_fruit_node class, which is called in find_fruit.cpp
	
	class can be used by simply calling find_fruit_node::start()
*/

#ifndef FIND_FRUIT_H
#define FIND_FRUIT_H

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <cv_bridge/cv_bridge.h>
#include <iostream>
#include "HSV_limits.h"
#include <math.h> //round()
#include "grab_fruit/fruit_pos.h" //custom defined message

class find_fruit_node
{
    public:
	find_fruit_node();
	~find_fruit_node();
	virtual void start();
    protected:
	//function prototypes
	void find_center(const cv::Mat &);
	void imageCallback(const sensor_msgs::ImageConstPtr&);
	void publish_fruit_pos(ros::Publisher &);
	//variables
	HSV_limits color_limits; 
	unsigned int fruit_x;
	unsigned int fruit_y;
};

find_fruit_node::find_fruit_node():fruit_x(0),fruit_y(0)
{}

find_fruit_node::~find_fruit_node()
{
	cv::destroyWindow("view");
	cv::destroyWindow("Track Bars");
}

void find_fruit_node::start() //will not return until node is shut down
{
	//camera subscriber
	ros::NodeHandle cam_nh;
	cv::namedWindow("view");
	cv::startWindowThread();
	image_transport::ImageTransport it(cam_nh);
	image_transport::Subscriber sub = it.subscribe("cameras/left_hand_camera/image", 1, &find_fruit_node::imageCallback, this);

	//fruit position publisher
	ros::NodeHandle pub_nh;
	ros::Publisher fruit_pos_pub = pub_nh.advertise<grab_fruit::fruit_pos>("fruit_position", 1);
	ros::Rate loop_rate(100);

	while (ros::ok())
	{
		publish_fruit_pos(fruit_pos_pub);
		loop_rate.sleep();
		ros::spinOnce(); 
	}
}

//called when a new image arrives in the cameras/left_hand_camera/image topic
void find_fruit_node::imageCallback(const sensor_msgs::ImageConstPtr& msg)
{
	//capture original image in bgr format
	//convert ros image to cv::Mat
	cv::Mat original_image=cv_bridge::toCvShare(msg, "bgr8")->image;
	//convert the bgr image to HSV
	cv::Mat HSV_image;
	cv::cvtColor(original_image,HSV_image,cv::COLOR_BGR2HSV);  

	//create an image that is white when in the HSV limits, and black everywhere else
	cv::Mat thresh_image; //640 x 400   (x x y)
	cv::inRange(HSV_image,
	cv::Scalar(color_limits.LowH,color_limits.LowS,color_limits.LowV),
	cv::Scalar(color_limits.HighH,color_limits.HighS,color_limits.HighV),
	thresh_image);
	//morphological opening (remove small objects from the foreground)
	cv::erode( thresh_image,thresh_image,getStructuringElement(cv::MORPH_ELLIPSE,cv::Size(5,5)));
	cv::dilate(thresh_image,thresh_image,getStructuringElement(cv::MORPH_ELLIPSE,cv::Size(5,5))); 

	//morphological closing (fill small holes in the foreground)
	cv::dilate(thresh_image,thresh_image,getStructuringElement(cv::MORPH_ELLIPSE,cv::Size(5,5))); 
	cv::erode( thresh_image,thresh_image,getStructuringElement(cv::MORPH_ELLIPSE,cv::Size(5,5)));

	//average all the xs and ys of the points
	find_center(thresh_image);

	try
	{
		//show the threshholded image
		cv::imshow("view",thresh_image); 
		cv::waitKey(30);
	}
	catch (cv_bridge::Exception& e)
	{
		ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
	}

	//if you press x on the window, the node shuts down
	if (cv::getWindowProperty("view",1)<0) //getWindowProperty returns -1 if there's no window
		ros::shutdown();
}

//finds the center pixel given an image, stores it in globar variables
void find_fruit_node::find_center(const cv::Mat & thresh_image)
{
	uint8_t *data_pointer=thresh_image.data;
	unsigned int number_of_white_pixels=0;
	double sum_x=0;
	double sum_y=0;
	for (int y=0;y<thresh_image.rows;y++)
	{
		for (int x=0;x<thresh_image.cols;x++)
		{
			//if the pixel is white
			if (data_pointer[thresh_image.cols*y+x]>0)
			{
				number_of_white_pixels++;
				sum_x+=x;
				sum_y+=y;
			}
		}
	}
	fruit_x=round(sum_x/(double)number_of_white_pixels);
	fruit_y=round(sum_y/(double)number_of_white_pixels);
}

//publish the fruit position
void find_fruit_node::publish_fruit_pos(ros::Publisher & fruit_pos_pub)
{
	//create the message
	grab_fruit::fruit_pos message;
	message.header.stamp=ros::Time::now();
	message.x=fruit_x;
	message.y=fruit_y;
	//publish the fruit position
	fruit_pos_pub.publish(message);
}
#endif
