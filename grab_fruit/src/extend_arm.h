/*
DESCRIPTION:
	Extend the arm towards a fruit. Contains code for moving arm, not code for computer vision.
	DELETE THIS: rossrv show SolvePositionIK. Look in to /robot/limb/left/inverse_dynamics_command
AUTHOR:
	Josh Munic
E-MAIL:
	jpmunic@gmail.com
*/
#ifndef EXTEND_ARM_H
#define EXTEND_ARM_H

#include <ros/ros.h>
#include <iostream>
#include "std_msgs/Bool.h"
#include "std_msgs/String.h"
#include "sensor_msgs/JointState.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/PoseStamped.h"
#include "baxter_core_msgs/SolvePositionIK.h"
#include "baxter_core_msgs/EndpointState.h"
#include "JointCommand.h"
#include "grab_fruit/fruit_pos.h"
#include "motor_controls.h"

class Extend_arm_node{
    public:
	Extend_arm_node();
	~Extend_arm_node();
	virtual void start();

    protected:
	//function prototypes
	void move_to_initial_position();
	void move_left_arm();
	void fruit_pos_callback(const grab_fruit::fruit_pos);
	void endpoint_pos_callback(const baxter_core_msgs::EndpointState); 
	geometry_msgs::PoseStamped get_stamped_endpoint();
	//variables
	unsigned int fruit_x;
	unsigned int fruit_y;
	geometry_msgs::Pose endpoint;
	Motor_controls motor_controls;
};

Extend_arm_node::Extend_arm_node():fruit_x(0),fruit_y(0){
	//initialize endpoint
	endpoint.position.x=0;
	endpoint.position.y=0;
	endpoint.position.z=0;
	endpoint.orientation.x=0;
	endpoint.orientation.y=0;
	endpoint.orientation.z=0;
	endpoint.orientation.w=0;
}

Extend_arm_node::~Extend_arm_node(){
	//disable baxter's motors
	motor_controls.disable();
}

void Extend_arm_node::start(){
	//enable baxter's motors
	motor_controls.enable();
	//move the arm into the initial position
	move_to_initial_position();
	
	//set up the fruit position subscriber
	ros::NodeHandle fruit_pos_nh;
	ros::Subscriber fruit_pos_sub=fruit_pos_nh.subscribe("fruit_position", 1, &Extend_arm_node::fruit_pos_callback,this);
	//set up the endpoint_subscriber
	ros::NodeHandle endpoint_pos_nh;
	ros::Subscriber endpoint_pos_sub=endpoint_pos_nh.subscribe("/robot/limb/left/endpoint_state",1, &Extend_arm_node::endpoint_pos_callback,this);
	//set up the inverse kinematics request client 
	ros::NodeHandle ik_nh; //inverse kinematics node handle
	ros::ServiceClient ik_client=ik_nh.serviceClient<baxter_core_msgs::SolvePositionIK>("/ExternalTools/left/PositionKinematicsNode/IKService");
	//set the loop rate
	ros::Rate loop_rate(10); //10 Hz
	//main loop
	while (ros::ok()){
		ros::spinOnce();
		/*
		baxter_core_msgs::SolvePositionIK srv;
		srv.request.pose_stamp.push_back(get_stamped_endpoint());
		srv.request.seed_mode=0;
		if (ik_client.call(srv)){
			std::cout<<"endpoint position: ";
			std::cout<<endpoint.position.x<<' ';
			std::cout<<endpoint.position.y<<' ';
			std::cout<<endpoint.position.z<<' ';
			std::cout<<endpoint.orientation.x<<' ';
			std::cout<<endpoint.orientation.y<<' ';
			std::cout<<endpoint.orientation.z<<' ';
			std::cout<<endpoint.orientation.w<<std::endl;
			if (srv.response.result_type[0]!=0){ 
				std::cout<<"joint angles: ";
				std::cout<<srv.response.joints[0].position[0]<<' ';
				std::cout<<srv.response.joints[0].position[1]<<' ';
				std::cout<<srv.response.joints[0].position[2]<<' ';
				std::cout<<srv.response.joints[0].position[3]<<' ';
				std::cout<<srv.response.joints[0].position[4]<<' ';
				std::cout<<srv.response.joints[0].position[5]<<' ';
				std::cout<<srv.response.joints[0].position[6]<<std::endl;
			}
			else{
				std::cout<<"invalid position"<<std::endl;
			}
		}
		else{
			ROS_ERROR("Failed to call service");
		}
		*/
		move_left_arm();
		loop_rate.sleep();
	}
}

void Extend_arm_node::move_to_initial_position(){
	std::vector<double> initial_joint_positions;
	//set the joint position to move to.
	//edit these values to change position.
	initial_joint_positions.push_back(1.11387);
	initial_joint_positions.push_back(-0.947225);
	initial_joint_positions.push_back(-1.62135);
	initial_joint_positions.push_back(2.43968);
	initial_joint_positions.push_back(-2.959);
	initial_joint_positions.push_back(0.655055);
	initial_joint_positions.push_back(0.613372);
	//move to the inital joint position
	const float publish_time=8.0; //seconds
	motor_controls.move_left_arm(initial_joint_positions,publish_time);
}

void Extend_arm_node::move_left_arm(){
	std::vector<double> joint_positions;
	//set the joint position to move to.
	//edit these values to change position.
	joint_positions.push_back(1.11387);
	joint_positions.push_back(-0.947225);
	joint_positions.push_back(-1.62135);
	joint_positions.push_back(2.43968);
	joint_positions.push_back(-2.959);
	joint_positions.push_back(0.655055);
	joint_positions.push_back(0.613372);
	//move to the inital joint position
	motor_controls.move_left_arm(joint_positions);
}

void Extend_arm_node::fruit_pos_callback(const grab_fruit::fruit_pos point)
{
	fruit_x=point.x;
	fruit_y=point.y;
	std::cout<<"x: "<<fruit_x<<" y: "<<fruit_y<<std::endl;
}

void Extend_arm_node::endpoint_pos_callback(const baxter_core_msgs::EndpointState endpoint_state){
	endpoint=endpoint_state.pose;
}

geometry_msgs::PoseStamped Extend_arm_node::get_stamped_endpoint(){
	geometry_msgs::PoseStamped output;
	output.pose=endpoint;//class member data
	output.header.seq=0;
	output.header.stamp=ros::Time::now();
	output.header.frame_id="base";
	return output;
	/*
	geometry_msgs::PoseStamped output;
	output.header.seq=0;
	output.header.stamp=ros::Time::now();
	output.header.frame_id="base";
	output.pose.position.x=0.657579481614;
	output.pose.position.y=0.851981417433;
	output.pose.position.z=0.0388352386502;
	output.pose.orientation.x=-0.366894936773;
	output.pose.orientation.y=0.885980397775;
	output.pose.orientation.z=0.108155782462;
	output.pose.orientation.w=0.262162481772;
	return output;
	*/
}
#endif
