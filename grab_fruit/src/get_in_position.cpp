/*
	Launches the get_in_position node. Moves Baxter to a position where its left hand camera faces forward.
*/

#include <ros/ros.h>
#include "get_in_position.h"
#include <iostream>

int main(int argc, char **argv)
{
	//initialize the node
	ros::init(argc, argv, "get_in_position");
	//create an instance of the get_in_position node
	get_in_position_node gip_node;
	//start the node
	gip_node.start();
	//wait for the user to press enter
	std::cout<<"press enter"<<std::endl;
	std::cin.get();
	return 0;
}
//created by Josh Munic (jpmunic@gmail.com) in 2016
