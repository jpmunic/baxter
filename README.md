# Baxter catch fruit #

### What is this repository for? ###

* A demo in which our Baxter anthropomorphic robot detects an apple (held by someone in front of him) and extends his arm to grasp the apple (vacuum gripper) and put it in a box

### How do I get set up? ###

* Install ROS and baxter workstation follow the website below.


+   http://sdk.rethinkrobotics.com/wiki/Workstation_Setup

* Go to src folder of your baxter working space and clone this repository
   
```
#!bash

cd ~/ros_ws/src
git clone https://bitbucket.org/jpmunic/baxter/overview#clone
```
* roslaunch

```
#!bash
roslaunch grab_fruit get_fruit.launch limb:=left

```